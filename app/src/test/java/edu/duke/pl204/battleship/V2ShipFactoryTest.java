package edu.duke.pl204.battleship;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class V2ShipFactoryTest {

  private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs){
    if(testShip.getName()!=expectedName){
      throw new IllegalArgumentException("The name is not the same as expected!");
    }
     for(Coordinate e : expectedLocs){
       if( testShip.occupiesCoordinates(e) ==  false){
           throw new IllegalArgumentException("Location not in ship!");
       }
      
       if(testShip.getDisplayInfoAt(e,true)!=expectedLetter){
          throw new IllegalArgumentException("Letter not match!");
        }
     }
  }
  @Test
  public void test_v2ShipFactory() {
    V2ShipFactory s = new V2ShipFactory();
    Placement pU = new Placement(new Coordinate(0, 0), 'U');
    Placement pR = new Placement(new Coordinate(0, 0), 'R');
    Placement pD = new Placement(new Coordinate(0, 0), 'D');
    Placement pL = new Placement(new Coordinate(0, 0), 'L');
    Ship<Character> bts_U = s.makeBattleship(pU);
    Ship<Character> bts_R = s.makeBattleship(pR);
    Ship<Character> bts_D = s.makeBattleship(pD);
    Ship<Character> bts_L = s.makeBattleship(pL);
    
    assertThrows(IllegalArgumentException.class,
        () -> checkShip(bts_U, "Battleshib", 'b', new Coordinate(0, 1), 
        new Coordinate(1, 0), new Coordinate(1, 1), new Coordinate(1, 2)));

    assertThrows(IllegalArgumentException.class,
        () -> checkShip(bts_U, "Battleship", 'd', new Coordinate(0, 1), 
        new Coordinate(1, 0), new Coordinate(1, 1), new Coordinate(1, 2)));

    assertThrows(IllegalArgumentException.class,
                 () -> checkShip(bts_U, "Battleship", 'b', new Coordinate(4, 4)));

    assertAll(
        () -> checkShip(bts_U, "Battleship", 'b', new Coordinate(0, 1), 
        new Coordinate(1, 0), new Coordinate(1, 1), new Coordinate(1, 2)));
    
    assertAll(
        () -> checkShip(bts_R, "Battleship", 'b', new Coordinate(0, 0), 
        new Coordinate(1, 0), new Coordinate(1, 1), new Coordinate(2, 0)));

    assertAll(
        () -> checkShip(bts_D, "Battleship", 'b', new Coordinate(0, 0), 
        new Coordinate(0, 1), new Coordinate(0, 2), new Coordinate(1, 1)));

    assertAll(
        () -> checkShip(bts_L, "Battleship", 'b', new Coordinate(0, 1), 
        new Coordinate(1, 0), new Coordinate(1, 1), new Coordinate(2, 1)));
    
    Ship<Character> cr_U = s.makeCarrier(pU);
    Ship<Character> cr_R = s.makeCarrier(pR);
    Ship<Character> cr_D = s.makeCarrier(pD);
    Ship<Character> cr_L = s.makeCarrier(pL);



    assertAll(
        () -> checkShip(cr_U, "Carrier", 'c', new Coordinate(0, 0),
        new Coordinate(1, 0), new Coordinate(2, 0), new Coordinate(2, 1),
        new Coordinate(3, 0), new Coordinate(3, 1), new Coordinate(4,1)));
    
    Placement pQ = new Placement(new Coordinate(0, 0), 'Q');
    assertThrows(IllegalArgumentException.class,
                 () -> s.makeCarrier(pQ));

    
  }
}
