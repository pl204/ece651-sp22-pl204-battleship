package edu.duke.pl204.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
  @Test
  public void test_rectangle_ship() {
    Coordinate c1 = new Coordinate(3,5);
    Placement p1 = new Placement(c1,'v');
    RectangleShip<Character> r1 = new RectangleShip<Character>("submarine", p1, 's', '*');
    assertEquals(true, r1.occupiesCoordinates(c1));
  }

  @Test
  public void test_inship(){
    Coordinate c1 = new Coordinate(3,5);
    Placement p1 = new Placement(c1,'v');
    Coordinate c2 = new Coordinate(3,7);
    RectangleShip<Character> r1 = new RectangleShip<Character>("submarine",p1,1,2,'s', '*');
    assertThrows(IllegalArgumentException.class, ()->r1.checkCoordinateInThisShip(c2));
  }
  
  @Test
  public void test_HitAt_wasHitAt(){
    Coordinate c1 = new Coordinate(3,5);
    Placement p1 = new Placement(c1,'v');
    Coordinate c2 = new Coordinate(3,6);
    RectangleShip<Character> r1 = new RectangleShip<Character>("submarine",p1,1,2,'s', '*');
    r1.recordHitAt(c1);
    assertEquals(true, r1.wasHitAt(c1));
    assertEquals(false, r1.wasHitAt(c2));
  }

  @Test
  public void test_isSunk(){
    Coordinate c1 = new Coordinate(3,5);
    Placement p1 = new Placement(c1,'v');
    Coordinate c2 = new Coordinate(3,6);
    RectangleShip<Character> r1 = new RectangleShip<Character>("submarine",p1,1,2,'s', '*');
     r1.recordHitAt(c1);
     assertEquals(false, r1.isSunk());
     r1.recordHitAt(c2);
     assertEquals(true, r1.isSunk());
  }

  @Test
  public void test_getname(){
    Coordinate c1 = new Coordinate(3,5);
    Placement p1 = new Placement(c1,'v');
    RectangleShip<Character> r1 = new RectangleShip<Character>("submarine",p1,1,2,'s', '*');
    assertEquals("submarine",r1.getName());
  }

  @Test
  public void test_getOldPlacement(){
    Coordinate c1 = new Coordinate(3,5);
    Placement p1 = new Placement(c1,'v');
    RectangleShip<Character> r1 = new RectangleShip<Character>("submarine",p1,1,2,'s', '*');
    assertEquals(p1,r1.getOldPlacement());
  }
 /*
  @Test
  public void test_changeCoordinate(){
    Coordinate c1 = new Coordinate(2,3);
    Coordinate c2 = new Coordinate(2,4);
    Coordinate c3 = new Coordinate(5,8);
    Placement p1 = new Placement(c1,'v');
    Placement p2 = new Placement(c3,'v');
    Placement p3 = new Placement(c3,'h');
    RectangleShip<Character> r1 = new RectangleShip<Character>("submarine",p1,1,2,'s', '*');
    
    Coordinate c4 = r1.changeCoordinate(c2, p2);
    assertEquals(new Coordinate(5,9),c4);

    Coordinate c5 = r1.changeCoordinate(c2, p3);
    assertEquals(new Coordinate(6,8),c5);

  }
  */
}

