package edu.duke.pl204.battleship;
import static org.junit.jupiter.api.Assertions.*;

import javax.swing.text.html.HTMLDocument.HTMLReader.CharacterAction;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  @Test
  public void test_witdth_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X', '*');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }
  
  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X', '*'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X', '*'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5,'X', '*'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20,'X', '*'));
  }
  
  @Test
  public void test_whatIsAt(){
    Placement c1 = new Placement("B0v");
    Placement c2 = new Placement("B2v");
    RectangleShip<Character> s1 = new RectangleShip<Character>("submarine",c1,'s','*');
    RectangleShip<Character> s2 = new RectangleShip<Character>("submarine",c2,'s','*');
    
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(3,4,'X','*');
    assertEquals(b1.tryAddShip(s1), null);
    assertEquals(b1.tryAddShip(s2), null);
    Character[][] expected = new Character[4][3];
    expected[1][0] = 's';
    expected[1][2] = 's';
    checkWhatIsAtBoard(b1, expected);
}
  private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected) {
    for (int i = 0; i < expected.length; i++) {
      for (int j = 0; j < expected[0].length; j++) {
        Coordinate c = new Coordinate(i, j);        
        assertEquals(b.whatIsAtForSelf(c), expected[i][j]);
      }
    }
  }
  @Test
  public void test_add_ship_check_rule(){
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 10, 'X','*');
    V2ShipFactory s = new V2ShipFactory();
     Placement place1 = new Placement(new Coordinate(3, 2), 'V');
     Placement place2 = new Placement(new Coordinate(3, 2), 'H');
     Ship<Character> sub = s.makeSubmarine(place1);
     b.tryAddShip(sub);
     Ship<Character> sub1 = s.makeSubmarine(place2);
     assertEquals("That placement is invalid: the ship overlaps another ship.", b.tryAddShip(sub1));
  }
  @Test
  public void test_fireAt(){
    V2ShipFactory sf =  new V2ShipFactory();
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(6,6,'X','*');
    Ship<Character> targetShip = sf.makeSubmarine(new Placement("A0H"));
    b.tryAddShip(targetShip);
    Ship<Character> hitShip = b.fireAt(new Coordinate(0,0));
    assertSame(hitShip,targetShip);

    assertFalse(targetShip.isSunk());
    b.fireAt(new Coordinate(0,1));
    assertTrue(targetShip.isSunk());

    b.fireAt(new Coordinate(4,3));
    assertEquals('X',b.whatIsAtForEnemy(new Coordinate(4,3)));
   }

  @Test
  public void test_all_sunk(){
    BattleShipBoard<Character> board = new BattleShipBoard<Character>(6, 6, 'X', '*');
    V1ShipFactory sf =  new V1ShipFactory();
    Ship<Character> ship1 = sf.makeSubmarine(new Placement("A0V"));
    Ship<Character> ship2 = sf.makeSubmarine(new Placement("C0V"));
    board.tryAddShip(ship1);
    board.tryAddShip(ship2);
    board.fireAt(new Coordinate(0,0));
    assertEquals(false, board.all_ship_sunk());
    board.fireAt(new Coordinate(1,0));
    assertEquals(false, board.all_ship_sunk());
    board.fireAt(new Coordinate(2,0));
    assertEquals(false, board.all_ship_sunk());
    board.fireAt(new Coordinate(3,0));
    assertEquals(true, board.all_ship_sunk());
   
  }
}
