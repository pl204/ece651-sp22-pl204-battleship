package edu.duke.pl204.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
  @Test
  public void test_display_empty_2by2() {
    Board<Character> b1 = new BattleShipBoard<Character>(2, 2,'X','*');
    BoardTextView view = new BoardTextView(b1);
    String expectedHeader = "  0|1\n";
    assertEquals(expectedHeader, view.makeHeader());
    String expected=
      expectedHeader+
      "A  |  A\n"+
      "B  |  B\n"+
      expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  @Test
  public void test_display_empty_3by2() {
    Board<Character> b1 = new BattleShipBoard<Character>(3, 2,'X','*');
    BoardTextView view = new BoardTextView(b1);
    String expectedHeader = "  0|1|2\n";
    String expectedBody =
      "A  | |  A\n"+
      "B  | |  B\n";
    emptyBoardHelper(3,2,expectedHeader,expectedBody);
  }

  @Test
  public void test_display_empty_3by5() {
    Board<Character> b1 = new BattleShipBoard<Character>(3, 5,'X','*');
    BoardTextView view = new BoardTextView(b1);
    String expectedHeader = "  0|1|2\n";
    String expectedBody =
      "A  | |  A\n"+
      "B  | |  B\n"+
      "C  | |  C\n"+
      "D  | |  D\n"+
      "E  | |  E\n";
    emptyBoardHelper(3,5,expectedHeader,expectedBody);
  }

  @Test
  public void test_display_empty_3by5_basic_ship() {
    Board<Character> b1 = new BattleShipBoard<Character>(3, 5,'X','*');
    Placement c1 = new Placement("A0v");
    Placement c2 = new Placement("D2v");
    RectangleShip<Character> s1 = new RectangleShip<Character>("submarine",c1,'s','*');
    RectangleShip<Character> s2 = new RectangleShip<Character>("submarine",c2,'s','*');
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);
    assertEquals('s',b1.whatIsAtForSelf(c1.getWhere()));
    assertEquals('s',b1.whatIsAtForSelf(c2.getWhere()));
    BoardTextView view = new BoardTextView(b1);
    String expectedHeader = "  0|1|2\n";
    String expectedBody =
      "A s| |  A\n"+
      "B  | |  B\n"+
      "C  | |  C\n"+
      "D  | |s D\n"+
      "E  | |  E\n";
    String expected = expectedHeader+expectedBody+expectedHeader;
    assertEquals(expected,view.displayMyOwnBoard());
    //emptyBoardHelper(3,5,expectedHeader,expectedBody);
    }


  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
    Board<Character> b1 = new BattleShipBoard<Character>(w, h,'X','*');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }
  
  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<Character>(11,20,'X','*');
    Board<Character> tallBoard = new BattleShipBoard<Character>(10,27,'X','*');
    //you should write two assertThrows here
    
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
  }

  @Test
  public void test_enemy_myBoard(){
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(4,3,'X','*');
    V1ShipFactory sf =  new V1ShipFactory();
    Ship<Character> s = sf.makeSubmarine(new Placement( new Coordinate(1,0),'h'));
    Ship<Character> s1 = sf.makeDestroyer(new Placement( new Coordinate(0,3),'v'));
    b.tryAddShip(s);
    b.tryAddShip(s1);
    String myView =
      "  0|1|2|3\n" +
      "A  | | |d A\n" +
      "B s|s| |d B\n" +
      "C  | | |d C\n" +
      "  0|1|2|3\n";
    assertEquals(myView, new BoardTextView(b).displayMyOwnBoard());    
    b.fireAt(new Coordinate(0,1));
     String enemyView =
             "  0|1|2|3\n" +
             "A  |X| |  A\n" +
             "B  | | |  B\n" +
             "C  | | |  C\n" +
             "  0|1|2|3\n";
     assertEquals(enemyView, new BoardTextView(b).displayEnemyBoard());
     b.fireAt(new Coordinate(1,1));
     enemyView =
             "  0|1|2|3\n" +
             "A  |X| |  A\n" +
             "B  |s| |  B\n" +
             "C  | | |  C\n" +
             "  0|1|2|3\n";
     assertEquals(enemyView, new BoardTextView(b).displayEnemyBoard());
  }
  @Test
  void test_tw0_boards_display(){
    String myView =
      "  0|1|2|3\n" +
      "A  |d|d|d A\n" +
      "B  | | |  B\n" +
      "C  | | |  C\n" +
      "  0|1|2|3\n";
      String enemyView =
      "  0|1|2|3\n" +
      "A  | | |  A\n" +
      "B  |s| |  B\n" +
      "C  |s| |  C\n" +
      "  0|1|2|3\n";
      Board<Character> b1 = new BattleShipBoard<Character>(4,3,'X','*');
      Board<Character> b2 = new BattleShipBoard<Character>(4,3,'X','*');
      V1ShipFactory sf = new V1ShipFactory();
      Ship<Character> myown = sf.makeDestroyer(new Placement("A1H"));
      Ship<Character> enemy = sf.makeDestroyer(new Placement("B1v"));
      b1.tryAddShip(myown);
      b2.tryAddShip(enemy);
      BoardTextView view1 = new BoardTextView(b1);
      BoardTextView view2 = new BoardTextView(b2);
      String expected ="\n"+
        "     my ocean                       enemy's ocean\n"+
        "  0|1|2|3                         0|1|2|3\n"+
        "A  |d|d|d A                     A  | | |  A\n"+
        "B  | | |  B                     B  | | |  B\n"+
        "C  | | |  C                     C  | | |  C\n"+
        "  0|1|2|3                         0|1|2|3\n";
      assertEquals(expected,view1.displayMyBoardWithEnemyNextToIt(view2, "my ocean", "enemy's ocean"));

  }
}
