package edu.duke.pl204.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
  @Test
  public void test_get_info() {
    ShipDisplayInfo<Character> s = new SimpleShipDisplayInfo<Character>('h','k');
    Coordinate c = new Coordinate(2,1);
    assertEquals('k',s.getInfo(c,true));
    assertEquals('h',s.getInfo(c,false));
  }

}
