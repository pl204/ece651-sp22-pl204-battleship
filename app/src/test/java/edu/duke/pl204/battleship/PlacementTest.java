package edu.duke.pl204.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
  @Test
  public void test_valid_case() {
    Placement p1 = new Placement("D3h");
    assertEquals(3,p1.getWhere().getRow());
    assertEquals(3,p1.getWhere().getColumn());
    assertEquals('H',p1.getOrientation());

    Placement p2 = new Placement("A9V");
    assertEquals(0,p2.getWhere().getRow());
    assertEquals(9,p2.getWhere().getColumn());
    assertEquals('V',p2.getOrientation());
    Coordinate c1 = new Coordinate("C3");
    Placement p3 = new Placement(c1,'v');
    assertEquals(2,p3.getWhere().getRow());
    assertEquals(3,p3.getWhere().getColumn());
    assertEquals('V',p3.getOrientation());
  }
  
   @Test
   public void test_error_case() {//only test 3 character
    assertThrows(IllegalArgumentException.class, () -> new Placement("B9"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("E"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("S3RR"));
  }

  @Test
  public void test_equals(){
    Placement p1 = new Placement("G2V");
    Placement p2 = new Placement("G2V");
    Placement p3 = new Placement("Y3H");
    Placement p4 = new Placement("Y2V");
    assertEquals(p1,p1);
    assertEquals(p1,p2);
    assertNotEquals(p2,p3);
    assertNotEquals(p3,p4);
    assertNotEquals(p3,"X2V");
  }

@Test
  public void test_toString(){
    Placement p = new Placement("E2H");
    assertEquals("(4, 2)H",p.toString());
  }

  
 @Test
  public void test_hashCode(){
    Placement p1 = new Placement("D4H");
    Placement p2 = new Placement("D4H");
    Placement p3 = new Placement("E5V");
    Placement p4 = new Placement("S2V");
    assertEquals(p1.hashCode(),p2.hashCode());
    assertNotEquals(p2.hashCode(),p3.hashCode());
    assertNotEquals(p2.hashCode(),p4.hashCode());
    assertNotEquals(p3.hashCode(),p4.hashCode());
  }


}
