package edu.duke.pl204.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
  @Test
  public void test_InboundCheck() {
    V1ShipFactory s = new V1ShipFactory();
    PlacementRuleChecker<Character> check = new InBoundsRuleChecker<Character>(null);
    BattleShipBoard<Character> board = new BattleShipBoard<Character>(6, 6, 'X','*');
    Placement place1 = new Placement(new Coordinate(5, 4), 'H');
    Ship<Character> c1 = s.makeCarrier(place1);
    assertEquals("That placement is invalid: the ship goes off the right of the board.", check.checkMyRule(c1, board));
    assertEquals("That placement is invalid: the ship goes off the right of the board.", check.checkPlacement(c1, board));

    Placement place2 = new Placement(new Coordinate(-2, 4), 'V');
    Ship<Character> c2 = s.makeCarrier(place2);
    assertEquals("That placement is invalid: the ship goes off the top of the board.", check.checkMyRule(c2, board));

    Placement place3 = new Placement(new Coordinate(4, 5), 'V');
    Ship<Character> c3 = s.makeCarrier(place3);
    assertEquals("That placement is invalid: the ship goes off the bottom of the board.", check.checkMyRule(c3, board));

    Placement place4 = new Placement(new Coordinate(3, -1), 'H');
    Ship<Character> c4 = s.makeCarrier(place4);
    assertEquals("That placement is invalid: the ship goes off the left of the board.", check.checkMyRule(c4, board));

    Placement place5 = new Placement(new Coordinate(0, 0), 'H');
    Ship<Character> c5 = s.makeCarrier(place5);
    assertEquals(null, check.checkMyRule(c5, board));
    assertEquals(null, check.checkPlacement(c5, board));
  }

}
