package edu.duke.pl204.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {

  private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs){
    if(testShip.getName()!=expectedName){
      throw new IllegalArgumentException("The name is not the same as expected!");
    }
     for(Coordinate e : expectedLocs){
       if( testShip.occupiesCoordinates(e) ==  false){
           throw new IllegalArgumentException("Location not in ship!");
       }
      
       if(testShip.getDisplayInfoAt(e,true)!=expectedLetter){
          throw new IllegalArgumentException("Letter not match!");
        }
     }
  }
  @Test
  public void test_v1ShipFactory() {
    V1ShipFactory s = new V1ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = s.makeDestroyer(v1_2);
    assertThrows(IllegalArgumentException.class,
        () -> checkShip(dst, "Dastry", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2)));
    assertThrows(IllegalArgumentException.class,
        () -> checkShip(dst, "Destroyer", 's', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2)));
   assertThrows(IllegalArgumentException.class, () -> checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2),
        new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2)));
   Ship<Character> sub = s.makeSubmarine(v1_2);
      assertThrows(IllegalArgumentException.class,
                   () -> checkShip(sub, "Submarine", 'c', new Coordinate(1, 2), new Coordinate(2, 2)) ); 
  Ship<Character> carr = s.makeCarrier(v1_2);
      assertThrows(IllegalArgumentException.class,
                   () -> checkShip(carr, "Csdfa", 'c', new Coordinate(1, 2), new Coordinate(2, 2)) );
      Ship<Character> bs = s.makeBattleship(v1_2);
      assertThrows(IllegalArgumentException.class,
                   () -> checkShip(bs, "battleship", 'b', new Coordinate(1, 6), new Coordinate(2, 2)) ); 

  V1ShipFactory s1 = new V1ShipFactory();
    Placement v1_2f = new Placement(new Coordinate(1, 2), 'f');
    assertThrows(IllegalArgumentException.class, () -> s.makeDestroyer(v1_2f));

  }

}
