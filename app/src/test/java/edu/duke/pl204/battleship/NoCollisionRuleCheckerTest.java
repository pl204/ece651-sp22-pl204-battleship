package edu.duke.pl204.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
  @Test
  public void test_noCollision() {
    PlacementRuleChecker<Character> check = new NoCollisionRuleChecker<Character>(null);
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 10, 'X', '*');
    V1ShipFactory s = new V1ShipFactory();
    Placement place1 = new Placement(new Coordinate(1, 3), 'V');
    Ship<Character> c = s.makeCarrier(place1);
    assertEquals(null, check.checkMyRule(c, b));
    b.tryAddShip(c);
    Placement place2 = new Placement(new Coordinate(1, 3), 'H');
    Ship<Character> c2 = s.makeCarrier(place2);
    assertEquals("That placement is invalid: the ship overlaps another ship.", check.checkMyRule(c2, b));
  }

  @Test
  public void test_combine(){
    PlacementRuleChecker<Character> rule1 = new InBoundsRuleChecker<Character>(null);
    PlacementRuleChecker<Character> rule2 = new NoCollisionRuleChecker<Character>(rule1);
    //pass case
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 10, 'X', '*');
    V1ShipFactory s = new V1ShipFactory();
    Placement place1 = new Placement(new Coordinate(1, 3), 'V');
    Ship<Character> c = s.makeCarrier(place1);
    assertEquals(null, rule2.checkPlacement(c,b));
  }
}
