package edu.duke.pl204.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class TextPlayerTest {
 @Test
void test_read_placement() throws IOException {
   ByteArrayOutputStream bytes = new ByteArrayOutputStream();
   TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
   String prompt = "Please enter a location for a ship:";
    Placement[] expected = new Placement[3];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    expected[1] = new Placement(new Coordinate(2, 8), 'H');
    expected[2] = new Placement(new Coordinate(0, 4), 'V');
    for (int i = 0; i < expected.length; i++) {
        Placement p = player.readPlacement(prompt);
        assertEquals(p, expected[i]); //did we get the right Placement back
        // assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
        bytes.reset(); //clear out bytes for next time around
    }
}

  @Test
  void test_getName() {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(3, 4, "A0H\n", bytes);
    String myname = player.getName();
    assertEquals("A",myname);
   }
  
  @Test
  void test_eof() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(3, 4, "", bytes);
    V1ShipFactory shipFactory = new V1ShipFactory();
    assertThrows(IOException.class, ()->player.doOnePlacement("Carrier", (p) -> shipFactory.makeCarrier(p)));
  }
     @Test
  void test_doOnePlacement() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(3, 4, "A0H\n", bytes);
    String question = "Player A where do you want to put a Destroyer?\n";
    String expectedHeader = "  0|1|2\n";
    String expectedBody =
      "A d|d|d A\n"+
      "B  | |  B\n"+
      "C  | |  C\n"+
      "D  | |  D\n";
    String expected = question+expectedHeader+expectedBody+expectedHeader;
    V2ShipFactory shipFactory = new V2ShipFactory();      
    player.doOnePlacement("Destroyer", (p)->shipFactory.makeDestroyer(p));
    assertEquals(expected,bytes.toString());
  }

  private TextPlayer createTextPlayer(int w, int h, String inputData, ByteArrayOutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h,'X','*');
    V2ShipFactory shipFactory = new V2ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory);
  }
 @Test
 void test_readCoordinate() throws IOException {
                                  
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2\nC8\na4\n", bytes);
    String prompt = "Player A where do you want to put a hit?";
    Coordinate[] expected = new Coordinate[3];
    expected[0] = new Coordinate(1, 2);
    expected[1] = new Coordinate(2, 8);
    expected[2] = new Coordinate(0, 4);
    for (int i = 0; i < expected.length; i++) {
        Coordinate p = player.readCoordinate(prompt);
        assertEquals(p, expected[i]); //did we get the right Placement back                                                          
        // assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline                                   
        bytes.reset(); //clear out bytes for next time around                                                                        
    }
    TextPlayer player1 = createTextPlayer(10, 20, "", bytes);
    assertThrows(IOException.class, ()->player1.readCoordinate(prompt));
}

}
