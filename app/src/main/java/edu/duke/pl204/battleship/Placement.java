package edu.duke.pl204.battleship;

public class Placement {
  public final Coordinate where;
  public final char orientation;
  public Coordinate getWhere() {

    return where;
  }
  public char getOrientation() {
    return Character.toUpperCase(orientation);
  }
  /**
   * This check if the letter is valid orientation, if is lowercase, convert to upper one.
   * 
   * @param c is the char indicating orientation
   * @return c is the uppercase char
   * @throws IllegalArgumentException if the char is not valid orientation
   */
  
  public Placement(String descr){
    /*
    if (descr == "\n"){
      throw new IllegalArgumentException("The input length must be 3 but was 0\n");
    }
    */
    if (descr.length()!=3){
      throw new IllegalArgumentException("The input length must be 3 but was" +descr.length()+"\n");
    }
    Coordinate w = new Coordinate(descr.substring(0,2));
    char ori = Character.toUpperCase(descr.charAt(2));
    //CheckOrientation(ori);
    this.where = w;
    this.orientation = ori;
  }

  /**
   * Consructor of placement, with coordinate and char as input.
   * 
   * @param w is the coordinate
   * @param or is the orientation char
   */
  public Placement(Coordinate w, char ori) {
    this.where = w;
    ori = Character.toUpperCase(ori);
    //CheckOrientation(ori);
    this.orientation = ori;
   }
  /*
  public void CheckOrientation(char ori){
    if (ori != 'V' && ori != 'H'){
      throw new IllegalArgumentException(
          "The orientation must be V and H, we got " + ori);
    }
  }
  */
  @Override
   public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Placement c = (Placement) o;
      return where.equals(c.getWhere()) && orientation == c.getOrientation();
    }
    return false;
  }
  @Override
  public String toString() {
    return where.toString()+orientation;
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

}
