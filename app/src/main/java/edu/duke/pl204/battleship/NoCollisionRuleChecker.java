package edu.duke.pl204.battleship;
/**
 * This class checks the rule of whether the ship placement will have collision
 */
public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T>{
  /**
   * Constructor for rule checkers
   * 
   * @param next is next rule checker
   */
  public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }
/**
   * Check if the ship is overlap with other of no ship
   * 
   * @param theShip  is the ship we want to check
   * @param theBoard is the board we want to put in
   * @return true if the ship placement is valid
   */
  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard){
    Iterable<Coordinate> where = theShip.getCoordinates();
    for (Coordinate c : where){
      if (theBoard.whatIsAtForSelf(c)!=null){
        return "That placement is invalid: the ship overlaps another ship.";
      }
    }      
    return null;
  }
}
