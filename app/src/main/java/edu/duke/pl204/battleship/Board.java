package edu.duke.pl204.battleship;

public interface Board<T> {
  public int getWidth();

  public int getHeight();

  public T whatIsAtForSelf(Coordinate where);

  public String tryAddShip(Ship<T> toAdd);

  public Ship<T> fireAt(Coordinate c);

  public T whatIsAtForEnemy(Coordinate where);

  public String whatIsShipName(Coordinate where);

  public boolean all_ship_sunk();

  public Ship<T> getShipAt(Coordinate where);

  public void updateShip(Ship<Character> oldShip, Ship<Character> newShip, Placement newPlacement);
}
