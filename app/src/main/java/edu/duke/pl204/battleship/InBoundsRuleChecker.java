package edu.duke.pl204.battleship;
/**
 * This class checks if a ship is in bound of board
 */

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T>{
  /**
   * Constructs a rule checker on next rule checker(recursion)
   * 
   * @param next is next rule checker
   */
  public InBoundsRuleChecker(PlacementRuleChecker<T> next){
  super(next);
  }
  
  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
    Iterable<Coordinate> where = theShip.getCoordinates();
    for (Coordinate c: where){
      if(c.getRow()<0){
        return "That placement is invalid: the ship goes off the top of the board.";
      }
      if(c.getRow()>= theBoard.getHeight()){
        return "That placement is invalid: the ship goes off the bottom of the board.";
      }
      if(c.getColumn()<0){
        return "That placement is invalid: the ship goes off the left of the board.";
      }
      if(c.getColumn()>=theBoard.getWidth()){
        return "That placement is invalid: the ship goes off the right of the board.";
      }
    }
    return null;
  }
  
}
