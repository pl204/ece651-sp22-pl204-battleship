package edu.duke.pl204.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.function.Function;
import java.util.Scanner;
//this class for play movement control
public class TextPlayer {
  public Board<Character> theBoard;
  private final BoardTextView view;
  private final BufferedReader inputReader;
  private final PrintStream out;
  final AbstractShipFactory<Character> shipFactory;
  private final String name;
  final ArrayList<String> shipsToPlace;
  final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;

  public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputSource, PrintStream out,
      V2ShipFactory s) {
    this.theBoard = theBoard;
    this.view = new BoardTextView(theBoard);
    this.inputReader = inputSource;
    this.out = out;
    this.shipFactory = s;
    this.name = name;
    this.shipsToPlace = new ArrayList<String>();
    this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
  }
  
    public String getName() {
    return name;
  }
  //read user input: read a placement
  public Placement readPlacement(String prompt) throws IOException {
    out.println(prompt);
    String s = inputReader.readLine();
    if (s == null) {
      throw new IOException("The input is null\n");
    }
    return new Placement(s);
  }

  public Coordinate readCoordinate(String prompt) throws IOException {
    out.println(prompt);
    String s = inputReader.readLine();
    if (s == null) {
      throw new IOException("The input is null\n");
    }
    return new Coordinate(s);
  }


  public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
    Placement p = readPlacement("Player " + this.name + " where do you want to put a " + shipName + "?");
    Ship<Character> s = createFn.apply(p);
    String msg;
      msg = theBoard.tryAddShip(s);
      if (msg!=null){
        throw new IllegalArgumentException(msg);  
    }
    out.print(view.displayMyOwnBoard());
  }

  public void doPlacementPhase() throws IOException {
    out.println(this.view.displayMyOwnBoard());
    out.println("Player " + this.name + ":" + "you are going to place the following ships (which are all\n"
        + "rectangular). For each ship, type the coordinate of the upper left\n"
        + "side of the ship, followed by either H (for horizontal) or V (for\n"
        + "vertical).  For example M4H would place a ship horizontally starting\n"
        + "at M4 and going to the right.  You have\n");

    out.println("2 \"Submarines\" ships that are 1x2");
    out.println("3 \"Destroyers\" that are 1x3");
    out.println("3 \"Battleships\" that are 1x4");
    out.println("2 \"Carriers\" that are 1x6");
    out.println();
    setupShipCreationMap();
    setupShipCreationList();
    
    for (Iterator<String> it = shipsToPlace.iterator(); it.hasNext();) {
      String s = it.next();
      
        try{
          doOnePlacement(s, shipCreationFns.get(s));
          }
        catch(Exception e){
          System.out.println(e.getMessage());
          doOnePlacement(s, shipCreationFns.get(s));
          }
      }
    }
  

  public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException {
    out.println("Player "+ this.name +"'s turn:");
    BoardTextView enemyView = new BoardTextView(enemyBoard);
    String enemyHeader = "Player " + enemyName + "'s ocean";
    String myHeader = "Your ocean";
    out.println(view.displayMyBoardWithEnemyNextToIt(enemyView, myHeader, enemyHeader));
    Coordinate fireCoordinate;
    String my_action="";
    my_action = readAction();
    if ("F".equalsIgnoreCase(my_action)){
    
      try{
      fireCoordinate = readCoordinate("Player " + this.name  + " where do you want to put a hit?");
      }
      catch(Exception e){
        System.out.println(e.getMessage());
        fireCoordinate = readCoordinate("Player " + this.name  + " where do you want to put a hit?");
      }
    
      Ship<Character> hitShip = enemyBoard.fireAt(fireCoordinate);
      if (hitShip == null){
        out.println("You missed!");
      }
      else{
        String shipName = hitShip.getName();
        out.println("You hit a " + shipName + "!");
      }
    }
    if ("M".equalsIgnoreCase(my_action)){
      doMove();
    }
    if ("S".equalsIgnoreCase(my_action)){
      out.println("You choose sonar.");
      Coordinate sonarCoordinate = readCoordinate("Where do you want to put sonar?");
      doSonar(enemyBoard, sonarCoordinate);
    }
  }
  
  protected void setupShipCreationMap() {
    shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
    shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
    shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
  }

  protected void setupShipCreationList() {
    shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
    shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
    shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
    shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
  }
  
  public String readAction() throws IOException{
    String prompt =
      "Possible actions for Player "+ name +" :\n\n" +
      " F Fire at a square\n" +
      " M Move a ship to another square (2 remaining)\n" +
      " S Sonar scan (1 remaining)\n\n" +
      "Player " + name + ", what would you like to do?\n" ;
     out.println(prompt);
     String ans = "";
     while(true){
       ans = inputReader.readLine();
       if (("F".equalsIgnoreCase(ans)) || ("M".equalsIgnoreCase(ans)) || ("S".equalsIgnoreCase(ans))){
         break;
       }
        else if (ans == null){
          throw new IOException("Input is null\n");  
        }
        else {
          out.println("You answer should be F/M/S, but is " + ans);
        }
     }
     return ans;
  }

  public void doSonar(Board<Character> enemyBoard, Coordinate sonarCoordinate){
    int sonar_row = sonarCoordinate.getRow();
    int sonar_col = sonarCoordinate.getColumn();
    int sub_counter = 0;
    int des_counter = 0;
    int bat_counter = 0;
    int car_counter = 0;
    for(int i=0; i<sonar_row +3; i++){
      for(int j=0; j<sonar_col+3; j++){
        if (Math.abs(i-sonar_row) + Math.abs(j-sonar_col) < 4){
          if(enemyBoard.whatIsShipName(new Coordinate(i,j))=="Submarine"){
            sub_counter ++;
          }
          if(enemyBoard.whatIsShipName(new Coordinate(i,j))=="Destroyer"){
            des_counter ++;
          }
          if(enemyBoard.whatIsShipName(new Coordinate(i,j))=="Battleship"){
            bat_counter ++;
          }
          if(enemyBoard.whatIsShipName(new Coordinate(i,j))=="Carrier"){
            car_counter ++;
      }
    }
  }
    }
    out.println("Submarines occupy " + sub_counter + " squares");
    out.println("Destroyers occupy " + des_counter + " squares");
    out.println("Battleships occupy " + bat_counter + " squares");
    out.println("Carriers occupy " + car_counter + " squares");
    out.println();
    //out.println("Press ENTER to continue");
    //Scanner scanner = new Scanner(System.in);
    //scanner.nextLine();
  }

  public boolean doMove() throws IOException{
    Ship<Character> toMove = null;
    out.println("You choose to move your ship.");
    Coordinate shipCoordinate = null;
    Placement shipPlacement = null;
    
    shipCoordinate = readCoordinate("Which ship does you want to move?");
    toMove = theBoard.getShipAt(shipCoordinate);
    if(toMove == null){
      out.println("No ship here");
      return false;
    }
    String ship_name = toMove.getName();
    try{
      shipPlacement = readPlacement("You select a " + ship_name + ", where do you want to move?");
    }
    catch(Exception e){
        System.out.println(e.getMessage());
        return false;
    }
    Ship<Character> new_ship = null;
    new_ship = shipCreationFns.get(ship_name).apply(shipPlacement);
    if (theBoard.tryAddShip(new_ship)!=null){
      return false;
    }
    theBoard.updateShip(theBoard.getShipAt(shipCoordinate), new_ship, shipPlacement);
    return true;
  }

}
