package edu.duke.pl204.battleship;

public class Coordinate {
  private final int row;
  private final int column;

  public int getRow() {
    return row;
  }

  
  public int getColumn() {
    return column;
  }
/**
   * A constructor that takes the row and column and initalizes the members.
   * @param r is row number
   * @param c is column number
   * @throws IllegalArgumentException is the row and column is smaller than 0.
   */

  public Coordinate(int r, int c) {
     this.row = r;
     this.column = c;
  }

  /**
   * check if two objects are equal
   * 
   * @param o is the object to check
   * @return a boolean
   */ 
  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Coordinate c = (Coordinate) o;
      return row == c.row && column == c.column;
    }
    return false;
  }
  @Override
  public String toString() {
    return "("+row+", " + column+")";
  }
  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  public Coordinate(String descr) {
    if (descr.length() != 2) {
      throw new IllegalArgumentException("The input string for coordinate must be 2 letter\n");
    }
    char rowLetter = descr.charAt(0);

    if(rowLetter>='a'&&rowLetter<='z'){
      rowLetter = Character.toUpperCase(rowLetter);
    }
    
    if (rowLetter < 'A' || rowLetter > 'Z') {
      throw new IllegalArgumentException("Row letter must be between A and Z, but is " + rowLetter+"\n");
    }
    
    char colNumChar = descr.charAt(1);
    if (colNumChar < '0' || colNumChar > '9') {
      throw new IllegalArgumentException("Column must be an integer between 0 to 9, but it is " + colNumChar+"\n");
    } else {
      int columnNumber = descr.charAt(1) - '0';
      this.column = columnNumber;
    }
   this.row = (int) rowLetter - 'A'; 

  }
}
