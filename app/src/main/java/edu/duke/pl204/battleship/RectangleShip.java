package edu.duke.pl204.battleship;

import java.util.HashMap;
import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T>{
  private final String name;

   public String getName(){
    return name;
  }

  public RectangleShip(String name, Placement oldPlacement, int width, int height, ShipDisplayInfo<T> m, ShipDisplayInfo<T> e) {
    super(oldPlacement, makeCoords(oldPlacement.getWhere(), width, height), m,e);
    this.name = name;
  }
  
  public RectangleShip(String name, Placement oldPlacement, int width, int height, T data, T onHit) {
    this(name, oldPlacement, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
  }
  
  public RectangleShip(String name, Placement oldPlacement, T data, T onHit){
    this("testship", oldPlacement, 1, 1, new SimpleShipDisplayInfo<T>(data, onHit),new SimpleShipDisplayInfo<T>(null, data));
  }
      
    //makeCoords: make coordinates into hashset
  public static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height){
    HashSet<Coordinate> coordsSet = new HashSet<Coordinate>();
    for(int i = 0; i< width; i++){
      for(int j = 0; j<height; j++){
        Coordinate c = new Coordinate(upperLeft.getRow()+i, upperLeft.getColumn()+j);
        coordsSet.add(c);
      }
    }
    return coordsSet;
  }


  public Coordinate changeCoordinate(Coordinate oldCoordinate, HashMap<Coordinate, Boolean> newPieces,Placement newPlacement){
    Placement oldPlacement = getOldPlacement();
    int self_row_diff = oldCoordinate.getRow() - oldPlacement.where.getRow();
    int self_col_diff = oldCoordinate.getColumn() - oldPlacement.where.getColumn();
    if (newPlacement.getOrientation() == oldPlacement.getOrientation()){
      return new Coordinate(newPlacement.where.getRow()+self_row_diff, newPlacement.where.getColumn()+self_col_diff);
    }
    else{
      return new Coordinate(newPlacement.where.getRow()+self_col_diff, newPlacement.where.getColumn()+self_row_diff);
    }
  }
  //
}

