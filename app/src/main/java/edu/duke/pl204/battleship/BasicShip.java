package edu.duke.pl204.battleship;

import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T> {
  public HashMap<Coordinate, Boolean> myPieces;
  protected ShipDisplayInfo<T> myDisplayInfo;
  protected ShipDisplayInfo<T> enemyDisplayInfo;
  protected Placement oldPlacement; 
  /**
   *Constructor for basic ship
   */
  public BasicShip(Placement oldPlacement,Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo,
                   ShipDisplayInfo<T> enemyDisplayInfo){
    this.myPieces = new HashMap<Coordinate, Boolean>();
    for (Coordinate c: where){
      myPieces.put(c, false);
    }
    this.myDisplayInfo = myDisplayInfo;
    this.enemyDisplayInfo = enemyDisplayInfo;
    this.oldPlacement = oldPlacement;
  }

  /**
   * Check if the given coordinate is ship.
   * 
   * @param where is a Coordinate to check if there is a Ship.
   * @return true if where is inside this ship, false if not.
   */

  @Override
  public boolean occupiesCoordinates(Coordinate where) {
    if (myPieces.get(where) == null){
      return false;
    }
    return true;
  }
  protected void checkCoordinateInThisShip(Coordinate c){
    if(myPieces.get(c)==null){
      throw new IllegalArgumentException("The coordinate is not in the ship!");
    }
  }
  
  @Override
  public boolean isSunk() {
    for(boolean v:myPieces.values()){
      if(v == false){
        return false;
      }
    }
    return true;
  }
  

  @Override
  public void recordHitAt(Coordinate where) {
    checkCoordinateInThisShip(where);
    myPieces.put(where, true);
  }

  @Override
  public HashMap<Coordinate, Boolean> getMyPieces(){
    return myPieces;
  }

  @Override
  public boolean wasHitAt(Coordinate where) {
    checkCoordinateInThisShip(where);
    if(myPieces.get(where)==true){
      return true;
    }
    else{
    return false;
    }
  }

  @Override
  public T getDisplayInfoAt(Coordinate where, boolean myship) {
    if(myship==true){
      return myDisplayInfo.getInfo(where,myPieces.get(where));
    }
    else{
      return enemyDisplayInfo.getInfo(where, myPieces.get(where));
    }
  }

   /**
   * Find all Coordinates that this Ship has.
   * @return An returns a Set<Coordinate>  with the coordinates that this Ship has
   */
  public Iterable<Coordinate> getCoordinates(){
    return myPieces.keySet();
  }

  public Placement getOldPlacement(){
    return oldPlacement;
  }

  public Coordinate changeCoordinate(Coordinate oldCoordinate, HashMap<Coordinate, Boolean> newPieces, Placement newPlacement){
    return null;
  }
}
