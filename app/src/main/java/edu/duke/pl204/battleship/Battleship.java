package edu.duke.pl204.battleship;

import java.util.HashMap;
import java.util.HashSet;

public class Battleship<T> extends BasicShip<T>{
  private final String name;

  public String getName(){
    return "Battleship";
  }

  public Battleship(Placement oldPlacement, Character orientation, ShipDisplayInfo<T> m, ShipDisplayInfo<T> e) {
    super(oldPlacement, makeCoords(oldPlacement.getWhere(), orientation), m,e);
    this.name = "Battleship";
  }
  
  public Battleship(Placement oldPlacement, Character orientation, T data, T onHit) {
    this(oldPlacement, orientation, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
  }

    //makeCoords: make coordinates into hashset
  public static HashSet<Coordinate> makeCoords(Coordinate upperLeft, Character orientation){
    HashSet<Coordinate> coordsSet = new HashSet<Coordinate>();
    int row = upperLeft.getRow();
    int col = upperLeft.getColumn();
    if (orientation == 'U'){
      coordsSet.add(new Coordinate(row,col+1));
      coordsSet.add(new Coordinate(row+1,col));
      coordsSet.add(new Coordinate(row+1,col+1));
      coordsSet.add(new Coordinate(row+1,col+2));
    }

    else if (orientation == 'R'){
      coordsSet.add(new Coordinate(row,col));
      coordsSet.add(new Coordinate(row+1,col));
      coordsSet.add(new Coordinate(row+1,col+1));
      coordsSet.add(new Coordinate(row+2,col));
    }

    else if (orientation == 'D'){
      coordsSet.add(new Coordinate(row,col));
      coordsSet.add(new Coordinate(row,col+1));
      coordsSet.add(new Coordinate(row,col+2));
      coordsSet.add(new Coordinate(row+1,col+1));
    }

    else if (orientation == 'L'){
      coordsSet.add(new Coordinate(row,col+1));
      coordsSet.add(new Coordinate(row+1,col));
      coordsSet.add(new Coordinate(row+1,col+1));
      coordsSet.add(new Coordinate(row+2,col+1));
    }
    else{
        throw new IllegalArgumentException(
          "The orientation must be U, R, D and L, we got " + orientation);
    }
    return coordsSet;
  }

    public Coordinate calculate_center(HashMap<Coordinate, Boolean> myPieces){
    double all_row = 0;
    double all_col = 0;
    for(Coordinate c: myPieces.keySet()){
      all_row += c.getRow();
      all_col += c.getColumn();
    }
    Coordinate newCoordinate = new Coordinate((int)Math.round(all_row/4),(int)Math.round(all_col/4));
    return newCoordinate;
  }

  public HashMap<Character, Coordinate> buildMyOwnMap(Coordinate center_point){
    Character ori = oldPlacement.getOrientation();
    HashMap<Character, Coordinate> mymap = new HashMap<Character, Coordinate>();
    int center_row = center_point.getRow();
    int center_col = center_point.getColumn();
    if (ori == 'U'){
      mymap.put('c', center_point);
      mymap.put('u', new Coordinate(center_row - 1, center_col));
      mymap.put('l', new Coordinate(center_row, center_col - 1));
      mymap.put('r', new Coordinate(center_row, center_col + 1));

    }
    if (ori == 'R'){
      mymap.put('c', center_point);
      mymap.put('u', new Coordinate(center_row, center_col + 1));
      mymap.put('l', new Coordinate(center_row + 1, center_col));
      mymap.put('r', new Coordinate(center_row - 1, center_col));
    }
    if (ori == 'D'){
       mymap.put('c', center_point);
       mymap.put('u', new Coordinate(center_row + 1, center_col));
       mymap.put('l', new Coordinate(center_row, center_col + 1));
       mymap.put('r', new Coordinate(center_row, center_col - 1));
    }
    if (ori == 'L'){
       mymap.put('c', center_point);
       mymap.put('u', new Coordinate(center_row, center_col - 1));
       mymap.put('l', new Coordinate(center_row + 1, center_col));
       mymap.put('r', new Coordinate(center_row - 1, center_col));
    }
    return mymap;
  }
 
  public Coordinate changeCoordinate(Coordinate oldCoordinate, HashMap<Coordinate, Boolean> newPieces, Placement newPlacement){
    Placement oldPlacement = getOldPlacement();
    Coordinate new_UpperLeft = oldPlacement.getWhere();
    HashMap<Character, Coordinate> old_mymap = new HashMap<Character, Coordinate>();
    Coordinate old_center_point = calculate_center(myPieces);
    old_mymap = buildMyOwnMap(old_center_point);
    Coordinate new_center_point = calculate_center(newPieces);
    HashMap<Character, Coordinate> new_mymap = new HashMap<Character, Coordinate>();
      new_mymap = buildMyOwnMap(new_center_point);
      for(Character key: old_mymap.keySet()){
        if (old_mymap.get(key).equals(oldCoordinate)){
          return new_mymap.get(key);
      }
    }
    return null;
  }
}
