package edu.duke.pl204.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character> {
  protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name){
    //check orientation
    if (where.getOrientation() != 'V' && where.getOrientation() != 'H'){
      throw new IllegalArgumentException(
          "The orientation must be V and H, we got " + where.getOrientation());
    }
    if (where.getOrientation() == 'V'){
      int swap = w;
      w = h;
      h = swap;
    }
    RectangleShip<Character> s = new RectangleShip<Character>(name, where, w, h, letter, '*');
    return s;
  }
  @Override
    public Ship<Character> makeSubmarine(Placement where) {
      return createShip(where, 1, 2, 's', "Submarine");
    }
  @Override
  public Ship<Character> makeBattleship(Placement where) {
    return createShip(where, 1, 4, 'b', "Battleship");
  }
  @Override
  public Ship<Character> makeCarrier(Placement where) {
    return createShip(where, 1, 6, 'c', "Carrier");
  }
  @Override
  public Ship<Character> makeDestroyer(Placement where) {
    return createShip(where, 1, 3, 'd', "Destroyer");
  }

}


