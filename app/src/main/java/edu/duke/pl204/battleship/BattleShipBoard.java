package edu.duke.pl204.battleship;

import java.util.ArrayList;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T> {
  private final int width;
  private final int height;
  private final ArrayList<Ship<T>> myShips;
  private final HashSet<Coordinate> enemyMisses;
  private final PlacementRuleChecker<T> placementChecker;
  private final T missInfo;
  private final T hitInfo;

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  /**
   * Constructs a BattleShipBoard with the specified width and height
   * 
   * @param w is the width of the newly constructed board.
   * @param h is the height of the newly constructed board.
   * @throws IllegalArgumentException if the width or height are less than or
   *                                  equal to zero.
   */
  public BattleShipBoard(int w, int h, T m, T hit) {
    if (w <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
    }
    if (h <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
    }
    this.width = w;
    this.height = h;
    this.myShips = new ArrayList<Ship<T>>();
    this.placementChecker = new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null));
    this.enemyMisses = new HashSet<Coordinate>();
    this.missInfo = m;
    this.hitInfo = hit;
  }

  /**
   * Adds ship to Arraylist myShips
   * 
   * @param toAdd is ship<T> (the ship that we want to add)
   * @return true if ship successfully added
   */

  public String tryAddShip(Ship<T> toAdd) {
    if (placementChecker.checkPlacement(toAdd, this) != null) {
      return placementChecker.checkPlacement(toAdd, this);
    } else {
      myShips.add(toAdd);
    }
    return null;
  }

  public T whatIsAtForSelf(Coordinate where) {
    return whatIsAt(where, true);
  }

  protected T whatIsAt(Coordinate where, boolean isSelf) {
    if (isSelf == false && enemyMisses.contains(where)) {
      return missInfo;
    }
    for (Ship<T> s : myShips) {
      if (s.occupiesCoordinates(where)) {
        return s.getDisplayInfoAt(where, isSelf);
      }
    }
    return null;
  }

  public String whatIsShipName(Coordinate where) {
    for (Ship<T> s : myShips) {
      if (s.occupiesCoordinates(where)) {
        return s.getName();
      }
    }
    return null;
  }

  public T whatIsAtForEnemy(Coordinate where) {
    return whatIsAt(where, false);
  }

  public Ship<T> fireAt(Coordinate c) {
    for (Ship<T> s : myShips) {
      if (s.occupiesCoordinates(c)) {
        s.recordHitAt(c);
        return s;
      }
    }
    enemyMisses.add(c);
    return null;
  }

  public boolean all_ship_sunk() {
    for (Ship<T> s : myShips) {
      if (s.isSunk() == false) {
        return false;
      }
    }
    return true;
  }

  public Ship<T> getShipAt(Coordinate where) {
    for (Ship<T> s : myShips) {
      if (s.occupiesCoordinates(where) == true) {
        return s;
      }
    }
    return null;
  }

  public void updateShip(Ship<Character> oldShip, Ship<Character> newShip, Placement newPlacement) {
    for (Coordinate oldShipCoords : oldShip.getCoordinates()) {
      if (whatIsAt(oldShipCoords, true) == hitInfo) {
        Coordinate newHit = oldShip.changeCoordinate(oldShipCoords, newShip.getMyPieces(), newPlacement);
        fireAt(newHit);
      }
    }
    myShips.remove(oldShip);
  }
}
