package edu.duke.pl204.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of a Board (i.e., converting it to a
 * string to show to the user). It supports two ways to display the Board: one
 * for the player's own board, and one for the enemy's board.
 */

public class BoardTextView {
  /**
   * The Board to display
   */
  private final Board<Character> toDisplay;

  /**
   * Constructs a BoardView, given the board it will display.
   * 
   * @param toDisplay is the Board to display
   * @throws IllegalArgumentException if the board is larger than 10x26.
   */
  public BoardTextView(Board<Character> toDisplay) {
    this.toDisplay = toDisplay;
    if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
      throw new IllegalArgumentException(
          "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
    }
  }

  /**
   * 
   * This makes the header line, e.g. 0|1|2|3|4\n
   * 
   * @return the String that is the header line for the given board
   */
  String makeHeader() {
    StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
    String sep = ""; // start with nothing to separate, then switch to | to separate
    for (int i = 0; i < toDisplay.getWidth(); i++) {
      ans.append(sep);
      ans.append(i);
      sep = "|";
    }
    ans.append("\n");
    return ans.toString();
  }

  public String displayMyOwnBoard() {
    return displayAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
  }

  public String displayEnemyBoard() {
    return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
  }

  public String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
    StringBuilder ans = new StringBuilder("");
    ans.append(makeHeader());
    for (int row = 0; row < toDisplay.getHeight(); row++) {
      ans.append((char) ('A' + row));
      ans.append(" ");
      Coordinate c = new Coordinate(row, 0);
      Character ch = getSquareFn.apply(c);
      if (ch != null) {
        ans.append(ch);
      } else {
        ans.append(" ");
      }
      for (int col = 1; col < (toDisplay.getWidth()); col++) {
        Coordinate c1 = new Coordinate(row, col);
        Character ch1 = getSquareFn.apply(c1);
        // Character ch1 = 's';
        if (ch1 != null) {
          ans.append("|");
          ans.append(ch1);
        } else {
          ans.append("| ");
        }
      }
      ans.append(" ");
      ans.append((char) ('A' + row));
      ans.append("\n");
    }
    ans.append(makeHeader());
    return ans.toString(); // this is a placeholder for the moment
  }

  public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
    String[] my_lines = this.displayMyOwnBoard().split("\n");
    String[] enemy_lines = enemyView.displayEnemyBoard().split("\n");
    StringBuilder str = new StringBuilder("");
    str.append("\n");
    str.append("     ");// space * 5
    str.append(myHeader);
    str.append(SpaceMaker(toDisplay.getWidth(), 'h'));
    str.append("     ");
    str.append(enemyHeader);
    str.append("\n");

    str.append(HeaderMaker(my_lines[0], enemy_lines[0]));

    for (int i = 1; i < my_lines.length - 1; i++) {
      str.append(my_lines[i]);
      str.append(SpaceMaker(toDisplay.getWidth(), 'b'));
      str.append(enemy_lines[i]);
      str.append("\n");
    }
    str.append(HeaderMaker(my_lines[my_lines.length - 1], enemy_lines[enemy_lines.length - 1]));

    return str.toString();
  }

  protected String HeaderMaker(String my_lines, String enemy_lines) {

    StringBuilder str = new StringBuilder("");
    str.append(my_lines);
    str.append("  ");
    str.append(SpaceMaker(toDisplay.getWidth(), 'b'));
    str.append(enemy_lines);
    str.append("\n");
    return str.toString();
  }

  protected String SpaceMaker(int w, Character i) {
    int len = 0;
    if (i == 'h') {
      len = 2 * w + 10;
    }
    if (i == 'b') {
      len = 2 * w + 13;
    }
    StringBuilder str = new StringBuilder("");
    for (int j = 0; j < len; j++) {
      str.append(" ");
    }
    return str.toString();
  }
}
