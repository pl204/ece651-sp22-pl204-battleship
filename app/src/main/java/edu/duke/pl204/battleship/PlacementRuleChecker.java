package edu.duke.pl204.battleship;
//This class is for checking each rules by Chain of Responsibility.
public abstract class PlacementRuleChecker<T> {
  private final PlacementRuleChecker<T> next;
  /**
   * Build a rule checker on next rule checker
   * 
   * @param next is next rule checker
   */

  public PlacementRuleChecker(PlacementRuleChecker<T> next) {
    this.next = next;
 }
  /**
   * Check ship's own rule.
   * 
   * @param theShip is the ship to that is going to put in
   * @param theBoard is the board for ship to place
   * @return true if the ship is place in the board bounds
   */

  protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

  
  public String checkPlacement (Ship<T> theShip, Board<T> theBoard) {
    //if we fail our own rule: stop the placement is not legal
    if (checkMyRule(theShip, theBoard)!=null) {
      return checkMyRule(theShip, theBoard);
    }
    //other wise, ask the rest of the chain.
    if (next != null) {
      return next.checkPlacement(theShip, theBoard); 
    }
    //if there are no more rules, then the placement is legal
    return null;
  }
}
